package model;

import java.util.regex.Pattern;

public class Phone extends ContactDetail {
    public Phone(String value) {
        super(value);
    }

    @Override
    public boolean validate() {
        String regex = "^\\d{10}$";
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(value).matches();
    }
}



