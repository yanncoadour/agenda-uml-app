package model;

public abstract class ContactDetail {
    protected String value;

    public ContactDetail(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public abstract boolean validate();
}
