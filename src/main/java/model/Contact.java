package model;

import java.util.ArrayList;
import java.util.List;

import java.util.ArrayList;
import java.util.List;

public class Contact {
    private String name;
    private List<ContactDetail> contactDetails;

    public Contact(String name) {
        this.name = name;
        this.contactDetails = new ArrayList<>();
    }

    // Getters, setters and other methods as needed

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ContactDetail> getContactDetails() {
        return contactDetails;
    }



    public void addContactDetail(ContactDetail contactDetail) {
        contactDetails.add(contactDetail);
    }



}
