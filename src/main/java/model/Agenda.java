package model;

import java.util.ArrayList;
import java.util.List;

public class Agenda {
    private List<Contact> contacts;
    private User owner;

    public Agenda() {
        this.owner = owner;
        this.contacts = new ArrayList<>();
    }


    public List<Contact> getContacts() {
        return contacts;
    }


    public void setOwner(User owner) {
        this.owner = owner;
    }

    public void addContact(Contact contact) {
        contacts.add(contact);
    }

    public void deleteContact(Contact contact){
        contacts.remove(contact);
    }

    public Contact findContactByName(String name) {
        for (Contact contact : contacts) {
            if (contact.getName().equals(name)) {
                return contact;
            }
        }
        return null;
    }
}
