package model;

import java.util.regex.Pattern;

public class Address extends ContactDetail {
    public Address(String value) {
        super(value);
    }

    @Override
    public boolean validate() {
        String regex = "^.{5,}$";
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(value).matches();
    }
}
