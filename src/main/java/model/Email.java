package model;

import java.util.regex.Pattern;

public class Email extends ContactDetail {
    public Email(String value) {
        super(value);
    }

    @Override
    public boolean validate() {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(value).matches();
    }
}
