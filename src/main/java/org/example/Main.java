package org.example;

import model.*;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Agenda agenda = new Agenda();

        while (true) {
            System.out.println("************************************************");
            System.out.println("*     Bienvenue dans l'application d'Agenda    *");
            System.out.println("************************************************");
            System.out.println("*     *                                        *" );
            System.out.println("*    * *           Tu es déjà dans un agenda   *" );
            System.out.println("*   * * *          Vu avec toi lors du cours   *");
            System.out.println("*  *  *  *         Utilisateur non fonctionnel.*" );
            System.out.println("* *********        Tu étais OK                 *" );
            System.out.println("*   ALERT                                      *" );
            System.out.println("************************************************                                           " );
            System.out.println("                                           " );
            System.out.println("Veuillez choisir une action : ");
            System.out.println("1. Ajouter un utilisateur");
            System.out.println("2. Ajouter un contact");
            System.out.println("3. Modifier un contact");
            System.out.println("4. Supprimer un contact");
            System.out.println("5. Afficher les contacts");
            System.out.println("6. Quitter");
            System.out.print("Votre choix: ");

            try {
                int choice = scanner.nextInt();
                scanner.nextLine(); // Consommer la ligne vide

                if (choice == 1) {
                    System.out.print("Entrez le login de l'utilisateur: ");
                    String login = scanner.nextLine();
                    System.out.print("Entrez le mot de passe de l'utilisateur: ");
                    String password = scanner.nextLine();
                    User user = new User(login, password);
                    agenda.setOwner(user);
                    System.out.println("Utilisateur ajouté avec succès!");
                } else if (choice == 2) {
                    System.out.println("--------------------------------");
                    System.out.println("Ajouter un contact :");
                    System.out.print("Entrez le nom du contact: ");
                    String name = scanner.nextLine();

                    Contact contact = new Contact(name);

                    System.out.print("Entrez l'adresse (minimum 5 caractères) :");
                    String addressValue = scanner.nextLine();
                    Address address = new Address(addressValue);
                    while (!address.validate()) {
                        System.out.print("Adresse invalide. Veuillez entrer une adresse valide : ");
                        addressValue = scanner.nextLine();
                        address = new Address(addressValue);
                    }
                    contact.addContactDetail(address);

                    System.out.print("Entrez le numéro de téléphone (format : 00000000000) : ");
                    String phoneValue = scanner.nextLine();
                    Phone phone = new Phone(phoneValue);
                    while (!phone.validate()) {
                        System.out.print("Numéro de téléphone invalide. Veuillez entrer un numéro de téléphone valide : ");
                        phoneValue = scanner.nextLine();
                        phone = new Phone(phoneValue);
                    }
                    contact.addContactDetail(phone);

                    System.out.print("Entrez l'email: ");
                    String emailValue = scanner.nextLine();
                    Email email = new Email(emailValue);
                    while (!email.validate()) {
                        System.out.print("Adresse email invalide. Veuillez entrer une adresse email valide : ");
                        emailValue = scanner.nextLine();
                        email = new Email(emailValue);
                    }
                    contact.addContactDetail(email);


                    System.out.print("Entrez le site web : ");
                    String websiteValue = scanner.nextLine();
                    Website website = new Website(websiteValue);
                    while (!website.validate()) {
                        System.out.print("Adresse web invalide. Veuillez entrer une adresse web valide : ");
                        websiteValue = scanner.nextLine();
                        website = new Website(websiteValue);
                    }
                    contact.addContactDetail(website);

                    agenda.addContact(contact);
                    System.out.println("Contact ajouté avec succès!");
                    System.out.println("--------------------------------");
                } else if (choice == 3) {
                    System.out.println("--------------------------------");
                    System.out.println("Modifier un contact :");
                    System.out.print("Entrez le nom du contact à modifier: ");
                    String name = scanner.nextLine();
                    Contact contact = agenda.findContactByName(name);

                    if (contact != null) {
                        System.out.print("Entrez le nouveau nom du contact (laissez vide pour ne pas modifier): ");
                        String newName = scanner.nextLine();
                        if (!newName.isEmpty()) {
                            contact.setName(newName);
                        }

                        for (ContactDetail detail : contact.getContactDetails()) {
                            System.out.println("Coordonnée actuelle: " + detail.getClass().getSimpleName() + ": " + detail.getValue());
                            System.out.print("Entrez la nouvelle valeur pour la coordonnée (laissez vide pour ne pas modifier): ");
                            String newValue = scanner.nextLine();
                            if (!newValue.isEmpty()) {
                                detail.setValue(newValue);
                            }
                        }

                        System.out.println("Contact modifié avec succès !");
                    } else {
                        System.out.println("Aucun contact trouvé avec ce nom.");
                    }
                    System.out.println("--------------------------------");

                } else if (choice == 4) {
                    System.out.println("--------------------------------");
                    System.out.println("Supprimez un contact :");
                    System.out.print("Entrez le nom du contact à supprimer: ");
                    String name = scanner.nextLine();
                    Contact contact = agenda.findContactByName(name);

                    if (contact != null) {
                        agenda.deleteContact(contact);
                        System.out.println("Contact supprimé avec succès !");
                    } else {
                        System.out.println("Aucun contact trouvé avec ce nom.");
                    }

                } else if (choice == 5) {
                    System.out.println("--------------------------------");
                    System.out.println("Liste des contacts :");
                    for (Contact contact : agenda.getContacts()) {
                        System.out.println("Nom: " + contact.getName());
                        for (ContactDetail detail : contact.getContactDetails()) {
                            System.out.println("Coordonnée: ");
                            System.out.println(" - " + detail.getClass().getSimpleName() + ": " + detail.getValue());
                        }
                        System.out.println();
                    }
                    System.out.println("--------------------------------");
                } else if (choice == 6) {
                    System.out.println("Merci d'avoir utilisé notre application d'Agenda. À bientôt !");
                    break;
                } else {
                    System.out.println("Choix invalide! Veuillez réessayer.");
                }
            } catch (InputMismatchException e) {
                System.out.println("Commande invalide! Veuillez entrer un nombre.");
                scanner.nextLine();
            }
        }


    }


}
