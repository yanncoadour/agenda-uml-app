# TRAVAIL DE YANN COADOUR ET FABIEN DEBIONNE 

![diagramme](./diagramme.png)


# Guide d'utilisation de l'artefact .jar pour l'application Agenda

L'application Agenda permet de gérer un carnet d'adresses contenant des contacts et leurs coordonnées. Cet artefact .jar est un exécutable qui vous permettra de lancer l'application directement depuis votre terminal.

## Prérequis

Pour utiliser l'artefact .jar, vous devez disposer de :

- Une installation de la JVM (Java Virtual Machine) version 19.
- Le fichier .jar de l'application Agenda.

## Installation

1. Téléchargez le fichier agenda-uml-app.jar de l'application Agenda. chemin : out/artifacts/agenda_uml_app_jar/agenda-uml-app.jar
2. Placez le fichier .jar dans un dossier de votre choix.
3. Ouvrez un terminal et rendez-vous dans le dossier contenant le fichier .jar à l'aide de la commande `cd`.
4. Exécutez la commande suivante pour lancer l'application :

```bash
java -jar agenda-uml-app.jar


